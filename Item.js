const mongoose = require('mongoose');
const ItemSchema = new mongoose.Schema({
    title: String,
    description: String,
    brand: String,
    serieNumber: String
})
module.exports = mongoose.model('Item', ItemSchema)