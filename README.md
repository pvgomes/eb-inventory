# Inventory API
Inventory API is an API that uses NodeJS and MongoDB and its create for use on AWS Elastic Beanstalk

### Tips
The ```.npmrc``` file fixes permission issues for the ```NPM sharp``` package and other packages which require elevated permissions, setting the 'unsafe-perm' flag to ```true```. Several packages are included in the ```package.json``` file which are not being used in this project to demonstrate the deployment works for image upload/resizing packages.     


### Run it locally?
We use docker to make our development process easier
`docker-compose up`


Enjoy
