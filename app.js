require('dotenv').config()
const express = require('express')
const http = require('http')

const app = express()
const connectToDatabase = require('./db')
const Item = require('./Item')

app.get('/', async (req, res) => {
    await connectToDatabase()
    const items = await Item.find()
    res.send({ v: 1, items })
})

http.createServer(app).listen(process.env.PORT || 5000)
